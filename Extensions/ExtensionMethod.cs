using tech_test_payment_api.Middleware;

namespace tech_test_payment_api.Extensions
{
    public static class ExtensionMethod
    {
        public static IApplicationBuilder AddExceptionHandler(this IApplicationBuilder app)
        {
            app.UseMiddleware<ExceptionHandlerMiddleware>();

            return app;
        }
    }
}