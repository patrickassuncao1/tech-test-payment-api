using Newtonsoft.Json;

namespace tech_test_payment_api.Utils
{
    public static class JsonCustomList<T>
    {
        public static List<T>? ListComplete(string filePath)
        {
            string fileContent = File.ReadAllText(filePath);

            List<T>? list = JsonConvert.DeserializeObject<List<T>>(fileContent);

            return list;
        }

        public static void UpdateList(List<T> list, string filePath)
        {
            string json = JsonConvert.SerializeObject(list, Formatting.Indented); ;
            File.WriteAllText(filePath, json);
        }

    }
}