using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Repositories;
using tech_test_payment_api.Models.DTO;
using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class SaleController : ControllerBase
    {
        private readonly SaleRepository _saleRepository;

        public SaleController()
        {
            _saleRepository = new SaleRepository();
        }


        [HttpPost("Create")]
        public IActionResult Create(CreateSaleDto sale)
        {
            var isTrue = _saleRepository.Create(sale: sale);
            return CreatedAtAction(nameof(GetById), new { id = sale.Id }, sale);;
        }

        [HttpGet("GetById/{id}")]
        public IActionResult GetById(int id)
        {
            var saleDto = _saleRepository.Find(id);

            return Ok(saleDto);
        }

        [HttpPatch("UpdateStatus/{id}")]
        public IActionResult UpdateStatus(int id, SaleStatusEnum status)
        {
            var sale = _saleRepository.UpdateStatus(id, status);
            return Ok(sale);
        }
    }
}