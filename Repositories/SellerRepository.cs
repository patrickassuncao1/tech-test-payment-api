using tech_test_payment_api.Models;
using tech_test_payment_api.Repositories.Interfaces;
using tech_test_payment_api.Utils;
using tech_test_payment_api.Exceptions;

namespace tech_test_payment_api.Repositories
{
    public class SellerRepository : ISellerRepository
    {
        private const string FILE_PATH = "Files/sellers.json";

        public List<Seller> GetAll()
        {
            return JsonCustomList<Seller>.ListComplete(FILE_PATH) ?? new List<Seller>();
        }

        public bool Create(Seller seller)
        {
            var listSeller = JsonCustomList<Seller>.ListComplete(FILE_PATH) ?? new List<Seller>();

            listSeller.Add(seller);

            JsonCustomList<Seller>.UpdateList(listSeller, filePath: FILE_PATH);

            return true;
        }

        public Seller Find(int id)
        {
            var listSeller = JsonCustomList<Seller>.ListComplete(FILE_PATH);

            if (listSeller == null)
            {
                throw new NotFoundException("Not Found");
            }

            return listSeller.Find(seller => seller.Id == id)!;

        }

    }
}