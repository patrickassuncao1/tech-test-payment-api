using tech_test_payment_api.Models.DTO;
using tech_test_payment_api.Enums;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Repositories.Interfaces
{
    public interface ISaleRepository
    {
        bool Create(CreateSaleDto sale);
        SaleDto? Find(int id);
        Sale UpdateStatus(int id, SaleStatusEnum status);
    }
}