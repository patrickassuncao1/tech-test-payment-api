using tech_test_payment_api.Models;

namespace tech_test_payment_api.Repositories.Interfaces
{
    public interface ISellerRepository
    {
        bool Create(Seller seller);
        List<Seller>? GetAll();
        Seller? Find(int id);

    }
}