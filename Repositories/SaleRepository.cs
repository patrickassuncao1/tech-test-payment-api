using tech_test_payment_api.Models.DTO;
using tech_test_payment_api.Utils;
using tech_test_payment_api.Models;
using tech_test_payment_api.Repositories.Interfaces;
using tech_test_payment_api.Enums;
using tech_test_payment_api.Exceptions;

namespace tech_test_payment_api.Repositories
{
    public class SaleRepository : ISaleRepository
    {
        private readonly SellerRepository _sellerRepository;
        private const string FILE_PATH = "Files/sales.json";

        public SaleRepository()
        {
            _sellerRepository = new SellerRepository();
        }

        public bool Create(CreateSaleDto sale)
        {

            if (sale.Items.Count() <= 0)
            {
                throw new BadRequestException("Por favor adicione pelo menos um item");
            }

            var newSale = new Sale();
            newSale.Id = sale.Id;
            newSale.SellerId = sale.Seller.Id;
            newSale.Status = 0;
            newSale.Items = sale.Items;

            var listSale = JsonCustomList<Sale>.ListComplete(FILE_PATH) ?? new List<Sale>();

            listSale.Add(newSale);

            _sellerRepository.Create(sale.Seller);

            JsonCustomList<Sale>.UpdateList(listSale, filePath: FILE_PATH);

            return true;
        }

        public SaleDto Find(int id)
        {
            var listSale = JsonCustomList<Sale>.ListComplete(FILE_PATH) ?? new List<Sale>();
            SaleDto saleDto = new SaleDto();
            Sale? sale;

            sale = listSale.Find(x => x.Id == id);

            if (sale == null)
            {
                throw new NotFoundException("Not Found");
            }

            var seller = _sellerRepository.Find(sale.SellerId);

            saleDto.Id = sale.Id;
            saleDto.Seller = seller!;
            saleDto.Items = sale.Items;
            saleDto.Status = sale.Status;

            return saleDto;
        }

        public Sale UpdateStatus(int id, SaleStatusEnum status)
        {

            int[] types = new int[2];
            var listCompleteSale = JsonCustomList<Sale>.ListComplete(FILE_PATH)!;
            Sale? findSale = null;

            foreach (var item in listCompleteSale)
            {
                if (item.Id == id)
                {
                    findSale = item;
                    break;
                }
            }

            if (findSale == null)
            {
                throw new NotFoundException("Not Found");
            }

            switch (findSale.Status)
            {
                case SaleStatusEnum.PendingPayment:
                    types[0] = (int)SaleStatusEnum.ApprovedPayment;
                    types[1] = (int)SaleStatusEnum.Canceled;
                    ValidationStatus(status: (int)status, types: types, currentStatus: findSale.Status);
                    break;
                case SaleStatusEnum.ApprovedPayment:
                    types[0] = (int)SaleStatusEnum.SentToCarrier;
                    types[1] = (int)SaleStatusEnum.Canceled;
                    ValidationStatus(status: (int)status, types: types, currentStatus: findSale.Status);
                    break;
                case SaleStatusEnum.SentToCarrier:
                    types[0] = (int)SaleStatusEnum.Delivered;
                    types[1] = (int)SaleStatusEnum.Delivered;
                    ValidationStatus(status: (int)status, types: types, currentStatus: findSale.Status);
                    break;
                default:
                    throw new BadRequestException($"Status atual: {findSale.Status}, Não foi possível atualizar o status");
            }

            findSale.Status = status;

            var newListSale = new List<Sale>();

            foreach (var item in listCompleteSale)
            {
                if (item.Id == findSale.Id)
                {
                    newListSale.Add(findSale);
                }
                else
                {
                    newListSale.Add(item);
                }
            }

            JsonCustomList<Sale>.UpdateList(newListSale, filePath: FILE_PATH);

            return findSale;
        }

        private int ValidationStatus(int status, int[] types, SaleStatusEnum currentStatus)
        {

            foreach (var item in types)
            {
                if (item == status)
                {
                    return status;
                }
            }

            throw new BadRequestException($"Status atual: {currentStatus} ,a opção escolhida não é apropriada");
        }


    }

}