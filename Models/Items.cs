namespace tech_test_payment_api.Models
{
    public class Items
    {
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}