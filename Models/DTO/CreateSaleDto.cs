namespace tech_test_payment_api.Models.DTO
{
    public class CreateSaleDto
    {
        public int Id { get; set; }
        public Seller Seller { get; set; }
        public List<Items> Items { get; set; }
    }
}