using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Models.DTO
{
    public class SaleDto : CreateSaleDto
    {
        public SaleStatusEnum Status { get; set; }
    }
}