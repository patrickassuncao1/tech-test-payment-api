using tech_test_payment_api.Enums;

namespace tech_test_payment_api.Models
{
    public class Sale
    {
        public int Id { get; set; }
        public int SellerId { get; set; }
        public List<Items> Items { get; set; }
        public SaleStatusEnum Status { get; set; }
        
    }
}