using tech_test_payment_api.Utils;
namespace tech_test_payment_api.Models
{
    public class Seller
    {
        public int Id { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}