namespace tech_test_payment_api.Exceptions
{
    public class BadRequestException : Exception
    {
        public BadRequestException(string msg): base(msg)
        {
            
        }
    }
}