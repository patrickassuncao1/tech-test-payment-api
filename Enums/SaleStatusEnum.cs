using System.Runtime.Serialization;
namespace tech_test_payment_api.Enums
{
    public enum SaleStatusEnum
    {
        PendingPayment,
        ApprovedPayment,
        Canceled,
        SentToCarrier,
        Delivered,

    }
}